import * as express from 'express';
import GenerateCtrl from './controllers/GenerateCtrl';

export default function setRoutes(app) {

    const router = express.Router();
    const genCtrl = new GenerateCtrl();

    // routes
    const baseApiRoute = '';
    const genApiRoute = '/generate';

    router.route(baseApiRoute).get(genCtrl.getConfig);

    router.route(genApiRoute).get(genCtrl.getTestFile);
    router.route(genApiRoute).post(genCtrl.postFileData);
    router.route(genApiRoute).put(genCtrl.putFile);

    app.use('/gen', router);
}
