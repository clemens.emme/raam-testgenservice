import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as express from 'express';
import * as log from 'fancy-log';
import * as helmet from 'helmet';
import * as http from 'http-error-handler';
import * as morgan from 'morgan';

import setRoutes from './routes';

const port = '8282';
const app = express();
config();
startServer();

/**
 * Configure server
 */
function config() {

    // middleware
    app.use(morgan('dev'));
    app.use(helmet());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(cors());
    app.options('*', cors());

    setRoutes(app);

    // error handler
    app.use(http.errorHandler);
    app.use(http.notFoundHandler);
}

/**
 * Start the server
 */
function startServer() {
    app.listen(port, () => {
        log.info(`Service running in ${app.get('env')} mode\n`);
    });
}

module.exports = app;
