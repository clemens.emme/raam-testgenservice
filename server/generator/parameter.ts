export class Parameter {
    public name: string;
    public type: string;
    public example: any;

    constructor(name: string) {
        this.name = name;
        this.type = '';
    }

    public setByExample(value: any) {
        this.type = typeof value;
        this.example = value;

        return this;
    }
}
