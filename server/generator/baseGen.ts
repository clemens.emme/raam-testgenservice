import {Parameter} from './parameter';

abstract class BaseGen {

    public readonly abstract CONTAINER_NAME: string;
    public readonly abstract CONTAINER_FORMAT: string;
    public readonly abstract NAME_EXAMPLE: string;

    public readonly abstract PARAMETER_LIST: Parameter[];

    public getGeneratorConfig(): any {
        return {
            name: this.CONTAINER_NAME,
            format: this.CONTAINER_FORMAT,
            parameters: this.PARAMETER_LIST,
        };
    }

    public fillUpGenObject(genObject: any): any {
        for (const param of this.PARAMETER_LIST) {
            if (!genObject[param.name]) {
                genObject[param.name] = param.example;
            }
        }

        return genObject;
    }

}

export default BaseGen;
