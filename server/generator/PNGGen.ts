import {Parameter} from './parameter';
import {PNG} from 'pngjs';
import BaseGen from './baseGen';

export default class PngGenerator extends BaseGen {
    public readonly CONTAINER_NAME = 'PNG Container';
    public readonly CONTAINER_FORMAT = 'png';

    public readonly NAME_EXAMPLE = 'blutig';
    public readonly WIDTH_EXAMPLE = 720;
    public readonly HEIGHT_EXAMPLE = 480;
    public readonly COLOR_EXAMPLE = '#000';

    public readonly PARAMETER_LIST = [
        new Parameter('name').setByExample(this.NAME_EXAMPLE),
        new Parameter('width').setByExample(this.WIDTH_EXAMPLE),
        new Parameter('height').setByExample(this.HEIGHT_EXAMPLE),
        new Parameter('color').setByExample(this.COLOR_EXAMPLE),
    ];

    // ==================================================================================================================================
    // REST Endpoints
    // ==================================================================================================================================

    public generateTestFile(): PNG {
        return this.generate(this.fillUpGenObject({}));
    }

    public generateFileByParams(parameter: any): PNG {
        console.log(parameter);
        return this.generate(this.fillUpGenObject(parameter));
    }

    // ==================================================================================================================================
    // Generation Methods
    // ==================================================================================================================================

    public generate(parameter: any): PNG {

        console.log(parameter.width);
        console.log(parameter.height);

        const png = new PNG({
            width: parameter.width,
            height: parameter.height,
            filterType: -1,
        });

        this.calcColorScheme(parameter.color);

        for (let y = 0; y < png.height; y++) {
            for (let x = 0; x < png.width; x++) {
                const idx = (png.width * y + x) << 2;
                png.data[idx] = 0;        // R
                png.data[idx + 1] = 0;    // G
                png.data[idx + 2] = 0;    // B
                png.data[idx + 3] = 255;  // A
            }
        }

        return png;
    }

    // TODO
    private calcColorScheme(color: string): any {
        const colorScheme = {r: 255, g: 255, b: 255, a: 255};
        let colorValues;

        // remove color code indicating #
        if (color.startsWith('#')) {
            color = color.replace('#', '');
        }

        // Split the color Value
        if (color.length % 3 === 0) {           // RGB Value
            colorValues = color.match(new RegExp('.{1,' + color.length / 3 + '}', 'g'));
            colorValues.map((x) => x.length === 1 ? x = x + x : x = x);

            colorScheme.r = parseInt(colorValues[0], 16);
            colorScheme.g = parseInt(colorValues[1], 16);
            colorScheme.b = parseInt(colorValues[2], 16);

        } else if (color.length % 4 === 0) {    // RGBA Value
            colorValues = color.match(new RegExp('.{1,' + color.length / 4 + '}', 'g'));
            for (let entry of colorValues) {
                if (entry.length === 1) entry += entry;
            }
            colorScheme.r = parseInt(colorValues[0], 16);
            colorScheme.g = parseInt(colorValues[1], 16);
            colorScheme.b = parseInt(colorValues[2], 16);
            colorScheme.a = parseInt(colorValues[3], 16);
        }

        // console.log(colorScheme);
    }
}
