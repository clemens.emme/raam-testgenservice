import BaseCtrl from './baseCtrl';
import {PNG} from 'pngjs';
import PngGenerator from '../generator/PNGGen';

export default class GenerateCtrl extends BaseCtrl {

    private generator = new PngGenerator();

    public getConfig = (req, res) => {
        const list = this.generator.getGeneratorConfig();
        res.send(list);
    };

    public getTestFile = (req, res) => {
        const png = this.generator.generateTestFile();

        res.writeHead(200, this.fileSendingHeader);
        png.pack().pipe(res);
    };

    public postFileData = (req, res) => {
        const png = this.generator.generateFileByParams(req.body);

        res.writeHead(200, this.fileSendingHeader);
        png.pack().pipe(res);
    };

    public putFile = (req, res) => {
        res.sendStatus(200);
    };

}
