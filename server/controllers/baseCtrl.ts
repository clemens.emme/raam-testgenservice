abstract class BaseCtrl {

    public fileSendingHeader = {'Content-Type': 'application/octet-stream'};

    // Get the config object
    public abstract getConfig: (req, res) => void;

    // Get generated test file (normal gen with default values)
    public abstract getTestFile: (req, res) => void;

    // Post data and generate file
    public abstract postFileData: (req, res) => void;

    // Put a file and return parameters extracted from that
    public abstract putFile: (req, res) => void;

}

export default BaseCtrl;
