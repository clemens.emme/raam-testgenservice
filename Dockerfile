# FROM mhart/alpine-node:10
FROM jfyne/node-alpine-yarn:latest
COPY . .
RUN yarn install --frozen-lockfile && yarn build

# FROM mhart/alpine-node:10
FROM jfyne/node-alpine-yarn:latest
ENV NODE_ENV=production
COPY --from=0 /usr/src/app/build .
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile
EXPOSE 8282
CMD ["node", "app"]
